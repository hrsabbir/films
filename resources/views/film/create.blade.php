@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <add-film inline-template>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Add Film') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('film.store') }}" enctype="multipart/form-data">


                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Film Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" v-model="name">


                                            <span v-if="errors.name!==undefined" class="text-danger">
                                                <strong>@{{ errors.name[0] }}</strong>
                                            </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Film Description') }}</label>

                                    <div class="col-md-6">
                                        <textarea id="description" rows='10' type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required v-model="description">{{ old('description') }}</textarea>

                                        <span v-if="errors.description!==undefined" class="text-danger">
                                                <strong>@{{ errors.description[0] }}</strong>
                                            </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="release_date" class="col-md-4 col-form-label text-md-right">{{ __('Release Date') }}</label>

                                    <div class="col-md-6">
                                        <input id="release_date" type="date" class="form-control{{ $errors->has('release_date') ? ' is-invalid' : '' }}" name="release_date" value="{{ old('release_date') }}" required v-model="release_date">

                                        <span v-if="errors.release_date!==undefined" class="text-danger">
                                                <strong>@{{ errors.release_date[0] }}</strong>
                                            </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="rating" class="col-md-4 col-form-label text-md-right">{{ __('Rating') }}</label>

                                    <div class="col-md-6">
                                        <input id="rating" type="text" class="form-control{{ $errors->has('rating') ? ' is-invalid' : '' }}" name="rating" value="{{ old('rating') }}" v-model="rating" required>

                                        <span v-if="errors.rating!==undefined" class="text-danger">
                                                <strong>@{{ errors.rating[0] }}</strong>
                                            </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="ticket_price" class="col-md-4 col-form-label text-md-right">{{ __('Ticket Price') }}</label>

                                    <div class="col-md-6">
                                        <input id="ticket_price" type="text" class="form-control{{ $errors->has('ticket_price') ? ' is-invalid' : '' }}" name="ticket_price" v-model="ticket_price" value="{{ old('ticket_price') }}" required>

                                        <span v-if="errors.ticket_price!==undefined" class="text-danger">
                                                <strong>@{{ errors.ticket_price[0] }}</strong>
                                            </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                    <div class="col-md-6">
                                        <select name="country" class="form-control" v-model="country">
                                            @foreach($countries as $index => $country)
                                                <option value="{{$index}}" @if($index==old('country')) selected @endif>{{$country['name']}}</option>
                                            @endforeach
                                        </select>

                                        <span v-if="errors.country!==undefined" class="text-danger">
                                                <strong>@{{ errors.country[0] }}</strong>
                                            </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="genres" class="col-md-4 col-form-label text-md-right">{{ __('Genres') }}</label>
                                    <div class="col-md-6">
                                        @foreach($genres as $genre)
                                            <div class="form-check">
                                                <input class="form-check-input" name='genres[]' type="checkbox" value="{{$genre->id}}" v-model="genres" >
                                                <label class="form-check-label" >
                                                    {{$genre->name}}
                                                </label>
                                            </div>
                                        @endforeach
                                            <span v-if="errors.genres!==undefined" class="text-danger">
                                                <strong>@{{ errors.genres[0] }}</strong>
                                            </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="photo" class="col-md-4 col-form-label text-md-right">{{ __('Photo') }}</label>

                                    <div class="col-md-6">
                                        <input id="photo" type="file" accept="image/*" class="form-control-file{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" value="{{ old('photo') }}" @change="setPhoto" required>

                                        <span v-if="errors.photo!==undefined" class="text-danger">
                                                <strong>@{{ errors.photo[0] }}</strong>
                                            </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary" @click.prevent="submit">
                                            {{ __('Add') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </add-film>
        </div>
    </div>
@endsection
