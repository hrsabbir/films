@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                   <films inline-template>
                       <div>
                           <div class="card"  v-for="film in filmsData.data">
                               <img class="card-img-top d-block mx-auto" style="max-width: 400px" :src="film.photo" >
                               <div class="card-body">
                                   <h5 class="card-title">@{{ film.name }}</h5>
                                   <p class="card-text">@{{ film.description }}</p>
                               </div>
                               <ul class="list-group list-group-flush">
                                   <li class="list-group-item">{{__('Released Date:')}} @{{ film.release_date }}</li>
                                   <li class="list-group-item">{{__('Rating: ')}} @{{ film.rating }} <i class="far fa-star"></i></li>
                                   <li class="list-group-item">{{__('Comments: ')}} @{{ film.comments.length }} </li>
                               </ul>
                               <div class="card-body">
                                   <a :href="'{{URL::to('/film')}}/'+film.slug" class="card-link">See Details</a>
                               </div>
                           </div>
                           <br>
                           <div class="text-center">
                              {{__('Total Films: ')}} @{{ filmsData.total }}
                           </div>
                           <nav aria-label="Page navigation example">
                               <ul class="pagination justify-content-center">
                                   <li class="page-item" v-if="filmsData.prev_page_url">
                                       <a class="page-link" @click.prevent="prevPage">Prev</a>
                                   </li>
                                   <li class="page-item disabled" v-else >
                                       <a class="page-link" href="#">Prev</a>
                                   </li>
                                   <li class="page-item disabled"><a class="page-link" href="#">@{{ filmsData.current_page }}</a></li>
                                   <li class="page-item" v-if="filmsData.next_page_url">
                                       <a class="page-link" @click.prevent="nextPage">Next</a>
                                   </li>
                                   <li class="page-item disabled" v-else >
                                       <a class="page-link" href="#">Next</a>
                                   </li>
                               </ul>
                           </nav>
                       </div>
                   </films>

            </div>
        </div>
    </div>
@endsection
