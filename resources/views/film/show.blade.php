@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
              <show-film :slug="{{json_encode($slug)}}" @auth :user="{{ json_encode(Auth::user()) }}"@else :user='-1' @endauth inline-template>
                  <div class="card mb-3 ">
                      <img class="card-img-top d-block mx-auto" style="max-width: 600px" :src="film.photo" >
                      <div class="text-right mr-2">
                          <button class="btn btn-danger" v-if="user.id!==undefined && user.id==film.user.id" @click="deleteFilm">Delete</button>
                      </div>
                      <div class="card-body ">
                          <h5 class="card-title">@{{film.name}}</h5>
                          <p class="card-text">@{{film.description}}</p>
                          <p class="card-text"><small class="text-muted">{{__('Released: ')}} @{{film.release_date}}</small></p>
                          <ul class="list-group list-group-flush">
                              <li class="list-group-item">{{__('Rating: ')}}@{{film.rating}} <i class="far fa-star"></i></li>
                              <li class="list-group-item">{{__('Ticket Price: ')}} @{{film.ticket_price}}</li>
                              <li class="list-group-item">{{__('Country: ')}} @{{film.country}}</li>
                              <li class="list-group-item">{{__('Genres: ')}}

                                      @{{ film.genres }}




                              </li>
                          </ul>
                          <p class="card-text mt-2"><small class="text-muted">{{__('Added By: ')}} @{{film.user.name}}</small> </p>
                      </div>
                      <comments :comments="film.comments"   :film="film.id" inline-template>
                          <div class="card-body">
                              <div class="d-flex mb-2"  style="background: #fafafa" v-for="comment in comments">
                                  <div class="m-3">
                                      <img src="http://www.gravatar.com/avatar/67e75efcdd743a5fa9b8bb12e0a94152.jpg?s=80&d=mm&r=g" alt="" class="d-block mx-auto mb-2 rounded-circle">
                                      @{{comment.user.name}}
                                  </div>
                                  <div class="ml-2 mt-4">
                                      <h5 >@{{comment.name}}</h5>
                                      <p>@{{comment.comment}}</p>
                                  </div>
                              </div>
                              <hr>
                              @auth
                                  <div class="row">
                                      <div class="col-sm-10">
                                          <input type="name" placeholder="Name" class="form-control mb-2" v-model="name" required >
                                          <strong class="text-danger" v-if="errors.name!==undefined">@{{ errors.name[0] }}</strong>
                                          <textarea name="" id="" rows="5" placeholder="Write your comment" class="form-control" v-model="comment">{{__('Write a Comment')}}</textarea>
                                          <strong class="text-danger" v-if="errors.comment!==undefined">@{{ errors.comment[0] }}</strong>
                                      </div>
                                      <div class="col-sm-2">
                                          <button class="btn btn-success" @click="submitComment">
                                              Comment
                                          </button>
                                      </div>
                                  </div>
                              @else
                                  <div class="d-block mb-2 mx-auto"  style="background: #fafafa">
                                      <h4>{{__('Please ')}} <a href="/login" class="">{{__('Login')}}</a> {{(' to Write a comment')}}</h4>
                                  </div>
                              @endauth
                          </div>
                      </comments>
                  </div>
              </show-film>
            </div>
        </div>
    </div>
@endsection
