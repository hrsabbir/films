<?php

namespace App;

use App\Helpers\CountriesHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Film extends Model
{
    use SoftDeletes;
    protected $fillable=['name','description','release_date','ticket_price','rating','photo','slug','country','genres'];
    protected $with=['comments','user'];
    protected $perPage = 1;

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getGenresAttribute($value){
        $genres=explode(',',$value);
        return implode(',',Genre::whereIn('id',$genres)->pluck('name')->toArray());
    }
    public function getCountryAttribute($value){
        foreach (CountriesHelper::getCountrylist() as $index=>$country){
            if($index==$value)
                return $country['name'];
        }
    }

    protected $dates = ['deleted_at'];
}
