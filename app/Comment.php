<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $fillable=['name','comment','user_id'];
    protected $with=['user'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function film(){
        return $this->belongsTo('App\Film');
    }

    protected $dates = ['deleted_at'];
}
