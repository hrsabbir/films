<?php

namespace App\Http\Requests;

use App\Genre;
use App\Helpers\CountriesHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddFilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country'=>[
                'required',
                'alpha',
                'min:2',
                'max:2',
                Rule::in(array_keys(CountriesHelper::getCountrylist()))
                ],
            'name'=>[
                'required',
                "regex:/^[a-zA-Z0-9 ]*$/",
                'min:1',
                'max:100',
                Rule::unique('films')->where(function ($query)  {
                    return $query
                        ->whereCountry($this->country)
                        ->whereName($this->name);
                }),
            ],
            'description'=> [
                'required',
                'string',
                'max:1000',
                'min:10',
            ],
            'release_date'=>[
                'required',
                'date',
            ],
            'rating'=>[
                'required',
                'numeric',
                'between:1,5'
            ],
            'ticket_price'=>[
                'required',
                'numeric',
                'between:0,1000'
            ],
            'genres'=>[
                'required'
            ],
            'genres.*'=>[
                Rule::in(Genre::all()->pluck('id')->toArray())
            ],
            'photo'=>[
                'required',
                'mimes:jpeg,jpg,png,gif',
                'dimensions:min_width=250,min_height=250'
            ]
        ];
    }
    public function messages()
    {
        return [
            'unique' => 'The :attribute is already been taken for the selected country',
        ];
    }
}
