<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store($id,Request $request){
        $this->validate($request,[
            'name'=>'required|string|min:2|max:100',
            'comment'=>'required|string|min:5|max:1000',
        ]);
        $film=Film::find($id);
        $comment=$film->comments()->create([
            'name'=>$request->name,
            'comment'=>$request->comment,
            'user_id'=>Auth::user()->id,
        ]);
        $comment=Comment::find($comment->id);
        return $comment;
    }
}
