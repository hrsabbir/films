<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use App\Helpers\CountriesHelper;
use App\Http\Requests\AddFilmRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('film.index');
    }
    public function getFilms(){
        return Film::paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            $genres=Genre::all();
            $countries=CountriesHelper::getCountrylist();
            return view('film.create')
                ->withGenres($genres)
                ->withCountries($countries);
        }
        return redirect()->to('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apistore(AddFilmRequest $request)
    {
        if(Auth::check()){
            $photo=$request->file('photo');
            $directory='/images/';
            $name=str_slug($request->name.'-'.$request->country).'.'.$photo->clientExtension();
            $photo->move(public_path($directory),$name);
            $photo=$directory.$name;

            $user=Auth::user();
            $user->films()->create([
                'name'=>$request->name,
                'description'=>$request->description,
                'rating'=>$request->rating,
                'ticket_price'=>$request->ticket_price,
                'country'=>$request->country,
                'genres'=>$request->genres,
                'release_date'=>$request->release_date,
                'slug'=>str_slug($request->name.'-'.$request->country),
                'photo'=>$photo,
            ]);
            return response()->json(['sucess'=>'true'],200);
        }

        return response()->json(['sucess'=>'false'],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('film.show')->withSlug($slug);
    }
    public function apiShow($slug){
        $film=Film::where('slug',$slug)->first();
        if($film==null)
            return response(['success'=>'false'],404);
        return $film;
    }
    public function apiUpdate($slug,AddFilmRequest $request){
        if(Auth::check()){
            $user=Auth::user();
            $user->films()->where('slug',$slug)->first()->update($request->all());
            return response()->json(['success'=>'true'],202);
        }
        return response()->json(['success'=>'false'],401);
    }

    public function apiDelete($slug)
    {
        if(Auth::check()){
            $film=Film::where('slug',$slug)->first();
            $user=Auth::user();
            if($film==null)
                return response(['success'=>'false'],404);
            if($film->user_id==$user->id){
                $film->comments()->delete();
                $film->delete();
                return response()->json(['success'=>'true'],202);
            }
        }
        return response()->json(['success'=>'false'],401);
    }

}
