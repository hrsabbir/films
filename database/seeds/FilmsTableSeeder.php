<?php

use App\Film;
use App\User;
use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $film=Film::create([
            'name'=>'Interstellar',
            'description'=>"In Earth's future, a global crop blight and second Dust Bowl are slowly rendering the planet uninhabitable. Professor Brand (Michael Caine), a brilliant NASA physicist, is working on plans to save mankind by transporting Earth's population to a new home via a wormhole. But first, Brand must send former NASA pilot Cooper (Matthew McConaughey) and a team of researchers through the wormhole and across the galaxy to find out which of three planets could be mankind's new home",
            'slug'=>'interstellar-us',
            'rating'=>'4.5',
            'ticket_price'=>'200',
            'country'=>'US',
            'release_date'=>'2014-10-26',
            'photo'=>'/images/interstellar-us.jpg',
            'user_id'=>User::first()->id,
            'genres'=>'5,8',
        ]);
        $film->comments()->create([
           'name'=>'Test 1',
           'comment'=>'Comment for Film 1',
           'user_id'=>User::first()->id,
        ]);
        $film=Film::create([
            'name'=>'Inception',
            'description'=>"Dom Cobb (Leonardo DiCaprio) is a thief with the rare ability to enter people's dreams and steal their secrets from their subconscious. His skill has made him a hot commodity in the world of corporate espionage but has also cost him everything he loves. Cobb gets a chance at redemption when he is offered a seemingly impossible task: Plant an idea in someone's mind. If he succeeds, it will be the perfect crime, but a dangerous enemy anticipates Cobb's every move.",
            'slug'=>'inception-us',
            'rating'=>'4.6',
            'ticket_price'=>'300',
            'country'=>'US',
            'photo'=>'/images/inception-us.jpg',
            'release_date'=>'2014-10-26',
            'user_id'=>User::first()->id,
            'genres'=>'3,6',
        ]);
        $film->comments()->create([
            'name'=>'Test 2',
            'comment'=>'Comment for Film 2',
            'user_id'=>User::first()->id,
        ]);

        $film=Film::create([
            'name'=>'The Martian',
            'description'=>"When astronauts blast off from the planet Mars, they leave behind Mark Watney (Matt Damon), presumed dead after a fierce storm. With only a meager amount of supplies, the stranded visitor must utilize his wits and spirit to find a way to survive on the hostile planet. Meanwhile, back on Earth, members of NASA and a team of international scientists work tirelessly to bring him home, while his crew mates hatch their own plan for a daring rescue mission.",
            'slug'=>'the-martian-us',
            'rating'=>'4.4',
            'ticket_price'=>'300',
            'country'=>'US',
            'release_date'=>'2014-10-26',
            'photo'=>'/images/the-martian-us.jpg',
            'user_id'=>User::first()->id,
            'genres'=>'5,9',
        ]);
        $film->comments()->create([
            'name'=>'Test 3',
            'comment'=>'Comment for Film 3',
            'user_id'=>User::first()->id,
        ]);
    }
}
