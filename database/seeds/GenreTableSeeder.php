<?php

use App\Genre;
use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::create(['name'=>'Action']);
        Genre::create(['name'=>'Horror']);
        Genre::create(['name'=>'Science Fiction']);
        Genre::create(['name'=>'Romance']);
        Genre::create(['name'=>'Drama']);
        Genre::create(['name'=>'Thriller']);
        Genre::create(['name'=>'Comedy']);
        Genre::create(['name'=>'Mystery']);
        Genre::create(['name'=>'Fantasy']);
    }
}
