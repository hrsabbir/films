<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect()->to('/films');
});

// RESTful Api for Film

Route::post('api/film/store','FilmController@apiStore');
Route::get('api/film/{slug}','FilmController@apiShow');
Route::get('api/film','FilmController@getFilms');
Route::patch('api/update/{slug}','FilmController@apiUpdate');
Route::delete('api/film/{slug}','FilmController@apiDelete');




Auth::routes(['verify' => true]);

Route::get('/home',  function () {
    return redirect()->to('/films');
});


Route::get('films','FilmController@index');

Route::resource('/film','FilmController')->except([
     'update', 'index','destroy'
]);
Route::post('comment/{id}','CommentController@store');

